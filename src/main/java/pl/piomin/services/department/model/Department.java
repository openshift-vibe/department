package pl.piomin.services.department.model;


public class Department {

	private String id;
	private String organizationId;
	private String name;

	public Department() {
		
	}

	public Department(String id, String organizationId, String name) {
		super();
		this.id = id;
		this.organizationId = organizationId;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "Department [id=" + id + ", organizationId=" + organizationId + ", name=" + name + "]";
	}

}
